	AREA RESET,DATA, READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0 ,= num
	MOV R1,#8;counter 
	MOV R2,#0;packed form
back    
	LDRB R3,[R0],#1
	LSL R2,#4
	ORR R2,R3
	SUB R1,#1
	TEQ R1,#0
	BNE back
	LDR R0,=res
	STR R2,[R0]
STOP B STOP
num DCB 0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08
	AREA mydata,DATA,READWRITE
res DCD 0