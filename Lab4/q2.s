	AREA RESET,DATA, READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,= bcd
	LDR R1,[R0]
	MOV R7,#8
	MOV R3,#0
up
	AND R2,R1,#0xF0000000
	LSL R1,#4
	LSR R2,#28
	MOV R4,#0x0A
	MLA R5,R3,R4,R2
	MOV R3,R5
	SUB R7,#1
	TEQ R7,#0
	BNE up
	LDR R8,=res
	STR R5,[R8]	
STOP B STOP
bcd DCD 0x12345678
	AREA mydata,DATA,READWRITE
res DCD 0