	AREA RESET,DATA, READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,= num
	MOV R1,#8
	MOV R2,#0
up
	LDRB R3,[R0]
	BL ascii_hex
	LSL R2,#4
	SUB R1,#1
	TEQ R1,#0
	BNE up
	LDR R0,=res
	STR R2,[R0]
STOP B STOP
num DCD 0x12345678
ascii_hex
	CMP R3,#0x41
	AREA mydata,DATA,READWRITE
res DCD 0