#include<lpc17xx.h>
#include<stdlib.h>
#include<time.h>
#define RS_CTRL 1<<27
#define EN_CTRL 1<<28
#define DT_CTRL 15<<23
unsigned long int temp1=0,temp2=0,j,i,temp,temp3,num;
unsigned char flag1=0,flag2=0,row,col,flag;
unsigned int x;
void lcd_write(void);
void port_write(void);
void scan(void);
void delay_lcd(unsigned int);
unsigned long int init_command[]={0x30,0x30,0x30,0x20,0x28,0x0C,0x06,0x01,0x80};
char arr[]={'0','1','2','3','4','5','6','7','8','9','+','-','='};
int main(void){
	SystemInit();
	SystemCoreClockUpdate();
	LPC_GPIO0->FIODIR=RS_CTRL|DT_CTRL|EN_CTRL;
	flag1=0;
	for(i=0;i<9;i++){
		temp1=init_command[i];
		lcd_write();
	}
	x=0;
	LPC_GPIO1->FIODIR=0;
	LPC_GPIO2->FIODIR=0x00003C00;
	while(1){
	if(x==0)
	{		
		for(row=0;row<4;row++){
			if(row==0)
				temp=1<<10;
			else if(row==1)
				temp=1<<11;
			else if(row==2)
				temp=1<<12;
			else if(row==3)
				temp=1<<13;
			LPC_GPIO2->FIOPIN=temp;
			flag=0;
			x++;
			scan();
			
			if(flag){
					flag1=1;
				num=row*4+col;
				temp1=arr[num];
				lcd_write();
				x=0;
			}
		}		
	}}
	while(1);
}
void lcd_write(void)
{
	flag2=(flag1==1)?0:((temp1==0x30)||(temp1==0x20))?1:0;
	temp2=temp1&0xF0;
	temp2<<=19;
	port_write();
	if(!flag2){
		temp2=temp1&0x0F;
		temp2<<=23;
		port_write();
	}
}
void port_write(void){
	LPC_GPIO0->FIOPIN=0;
	LPC_GPIO0->FIOPIN=temp2;
	if(flag1==0)
		LPC_GPIO0->FIOCLR=RS_CTRL;
	else
		LPC_GPIO0->FIOSET=RS_CTRL;
	LPC_GPIO0->FIOSET=EN_CTRL;
	delay_lcd(25);
	LPC_GPIO0->FIOCLR=EN_CTRL;
	delay_lcd(5000);
}
void delay_lcd(unsigned int r1){
	unsigned int r;
	for(r=0;r<r1;r++);
}
void scan(void){
	
	temp3 = LPC_GPIO1->FIOPIN;
	temp3&=0x07800000;
	if(temp3!=0){
		flag=1;
		if(temp3==1<<23)
			col=0;
		else if(temp3==1<<24)
			col=1;
		else if(temp3==1<<25)
			col=2;
		else if(temp3==1<<26)
			col=3;
	}
}