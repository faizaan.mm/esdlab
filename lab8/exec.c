#include <LPC17xx.h>
 #include <stdio.h>

 #define	FIRST_SEG	0xF87FFFFF
 #define	SECOND_SEG	0xF8FFFFFF
 #define	THIRD_SEG	0xF97FFFFF
 #define	FOURTH_SEG	0xF9FFFFFF
 #define	DISABLE_ALL 0xFA7FFFFF

 unsigned int dig1=0x00,dig2=0x00,dig3=0x00,dig4=0x00;
 unsigned long LED[4][4] = {{0x3F, 0x06, 0x5B, 0x4F}, {0x66, 0x6D, 0x7D, 0x07}, {0x7F, 0x6F, 0x77, 0x7C}, {0x39, 0x5E, 0x79, 0x71}};
 unsigned long int temp1 = 0x00000000,i=0,dig_count=0,j=0;
 int in=0x0000, val;
  int f1=1;
 int f2=1;
 int f3=1;
 
	 
 unsigned int temp2=0x00,temp;
 unsigned int flag=0,col,row;
 void delay(void);
 
 void Display(void);
 void scan();

 int main(void)
 {    
	SystemInit();
	SystemCoreClockUpdate();
	
	/*LPC_PINCON->PINSEL0 &= 0xFF0000FF;	 //P0.4 to P0.11 GPIO data lines
	LPC_PINCON->PINSEL3 &= 0xFFC03FFF;	 //P1.23 to P1.26 GPIO enable lines*/

	LPC_GPIO0->FIODIR |= 0x00000FF0;	//P0.4 to P0.11 output
	LPC_GPIO1->FIODIR |= 0x07800000;	//P1.23 to P1.26 output
	LPC_PINCON->PINSEL0 &= 0x3F0000FF;
	LPC_PINCON->PINSEL1 &= 0xFFFFC000;
	LPC_PINCON->PINSEL3 &= 0XFFC03FFF;
	LPC_GPIO0->FIODIRL |= 0x0FF0;
	LPC_GPIO0->FIODIRH |= 0x0078;
	LPC_GPIO1->FIODIRH |= 0x0780;
	LPC_GPIO0->FIOCLR = 0xFFFFFFFF;
	
	
	
	while(1)
	{
		while(1)
		{
		for(row=0;row<4;row++)
			{
				LPC_GPIO0->FIOCLR = 0x00780000;
				if(row==0)
				  LPC_GPIO0->FIOPINH = 0x0008;
				else if(row==1)
					LPC_GPIO0->FIOPINH = 0x0010;
				else if(row==2)
					LPC_GPIO0->FIOPINH = 0x0020;
				else
					LPC_GPIO0->FIOPINH = 0x0040;
				
				in = LPC_GPIO0->FIOPIN & 0x00078000;
				if(in==0x00008000)
					col = 0;
				else if(in==0x00010000)
					col = 1;
				else if(in==0x00020000)
					col = 2;
				else if(in==0x00040000)
					col=3;
				
				if(in!=0x00000000)
				{
					flag=1;

				
					delay();
				dig_count +=1;  
				if(dig_count == 0x05)
            dig_count = 0x00;
				Display();
					
				}
			
			}
			
		}
			
	} //end of while(1)
}
//end of main


void delay(void)
 {
	 unsigned int i;
	 for (i=0;i<1000;i++);
		
 }


 void Display(void)      //To Display on 7-segments
 {

        
	if(dig_count == 0x01)		// For Segment U8
	{
		if(row==1 && col== 2)
		{
			if(f1==1)
			{dig1= 0x06;
		temp1 = dig1; 
		LPC_GPIO1->FIOPIN = FIRST_SEG;
		f1++;}
		if(f1==2)
			{dig1= 0x5B;
		temp1 = dig1; 
		LPC_GPIO1->FIOPIN = FIRST_SEG;
		f1++;}
		if(f1==3)
			{dig1= 0x66;
		temp1 = dig1; 
		LPC_GPIO1->FIOPIN = FIRST_SEG;
		f1++;}
		if(f1==4)
			{dig1= 0x7F;
		temp1 = dig1; 
		LPC_GPIO1->FIOPIN = FIRST_SEG;
		f1++;}
		
			   }

	}

	else if(dig_count == 0x02)	// For Segment U9
	{
		if(row==1 && col== 1)
		{
			if(f2==1)
			{dig2= LED[0][1];
		temp1 = dig2; 
		LPC_GPIO1->FIOPIN = SECOND_SEG;
		f2++;}
		if(f2==2)
			{dig2= LED[0][2];
		temp1 = dig2; 
		LPC_GPIO1->FIOPIN = SECOND_SEG;
		f2++;}
		if(f2==3)
			{dig2= LED[1][0];
		temp1 = dig2; 
		LPC_GPIO1->FIOPIN = SECOND_SEG;
		f2++;}
		if(f2==4)
			{dig2= LED[2][0];
		temp1 = dig2; 
		LPC_GPIO1->FIOPIN = SECOND_SEG;
		f2++;}
		
			   }    
   
	}

	else if(dig_count == 0x03)	// For Segment U10
	{
		if(row==1 && col== 0)
		{
			if(f3==1)
			{dig3= LED[0][1];
		temp1 = dig3; 
		LPC_GPIO1->FIOPIN = THIRD_SEG;
		f2++;}
		if(f3==2)
			{dig3= LED[0][2];
		temp1 = dig3; 
		LPC_GPIO1->FIOPIN = THIRD_SEG;
		f2++;}
		if(f3==3)
			{dig3= LED[1][0];
		temp1 = dig3; 
		LPC_GPIO1->FIOPIN = THIRD_SEG;
		f2++;}
		if(f3==4)
			{dig3= LED[2][0];
		temp1 = dig3; 
		LPC_GPIO1->FIOPIN = THIRD_SEG;
		f2++;}
		
			   }    
	}
	else if(dig_count == 0x04)	// For Segment U11
	{ 
		dig4= LED[row][col];
		temp1 = dig4;   
		LPC_GPIO1->FIOPIN = FOURTH_SEG;    

	}
	LPC_GPIO0->FIOMASK=0xFFF87FFF;
	temp1=temp1<<4;
	LPC_GPIO0->FIOMASK=0xFFFFF00F;
	LPC_GPIO0->FIOPIN=temp1;
	
	
	
 }	
 void scan()
 {
	 unsigned long temp3;
	 temp3=LPC_GPIO1->FIOPIN;
	 temp&=0x07800000;
	 if(temp3!=0)
	 {
		 flag=1;
		 if(temp3==1<<23)
			 col=0;
		 else if(temp3==1<<24)
			 col=1;
		 else if(temp3==1<<25)
			 col=2;
		 else if(temp3==1<<26)
			 col=3;
	 }
 }