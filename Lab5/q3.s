	AREA RESET,DATA, READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R13,=0x10000040
	MOV R1,#5
	MOV R2,#4
	MOV R3,#9
	MOV R4,#1
	MOV R5,#3
	MOV R6,#7
	MOV R7,#2
	MOV R8,#8
	MOV R9,#10
	MOV R10,#6
	STMDB R13!,{R1-R10}
	MOV R0,R13
	MOV R8,#9
up1	MOV R7,R8
	MOV R1,R0
up2 LDR R2,[R0]
	LDR R3,[R1,#4]!
	CMP R2,R3
	BLS skip
	STR R2,[R1]
	STR R3,[R0]
skip
	SUB R7,#1
	TEQ R7,#0
	BNE up2
	ADD R0,#4
	SUB R8,#1
	TEQ R8,#0
	BNE up1
	
	LDM R13!,{R1-R10}
STOP B STOP
	AREA mydata,DATA,READWRITE
res DCD 0