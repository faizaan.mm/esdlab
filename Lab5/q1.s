	AREA RESET,DATA, READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,=arr
	LDR R1,=num
	LDR R7,[R1]
	LDR R5,=res
	MOV R2,#-1
	MOV R8,#-1
	MOV R4,#10
up
	LDR R3,[R0],#4
	MOV R6,R3
	SUB R4,#1
	ADD R8,#1
	CMP R3,R7
	BEQ exi
	TEQ R4,#0
	BNE up
	STR R2,[R5]
exi MOV R2,R8
	STR R2,[R5]
STOP B STOP

num DCD 5
arr DCD 0,1,2,3,4,5,6,7,8,9
	AREA mydata,DATA,READWRITE
res DCD 0