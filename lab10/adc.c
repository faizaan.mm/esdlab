#include<LPC17xx.h>
#include<stdio.h>
#define RS_CTRL 1<<27
#define EN_CTRL 1<<28
#define DT_CTRL 15<<23
unsigned long int temp1=0,temp2=0,i,j,x;
float result_an;
int result_dig;
char dig[7];
unsigned char flag1=0,flag2=0;
 char s[]={"DIFFERENCE: "};
void lcd_write(void);
void port_write(void);
void delay_lcd(unsigned int);
void ADC_IRQHandler(void);
	int channel,temp,result4,result5;
unsigned long int init_command[]={0x30,0x30,0x30,0x20,0x28,0x0C,0x06,0x01,0x80};

int main(void)
{
 SystemInit();
 SystemCoreClockUpdate();
 
 LPC_GPIO0->FIODIR=DT_CTRL|RS_CTRL|EN_CTRL;
 LPC_PINCON->PINSEL3=(3<<28)|(3<<30);
	LPC_ADC->ADCR=(1<<4)|(1<<5)|(1<<16)|(1<<21);
	LPC_ADC->ADINTEN=(1<<4)|(1<<5);
	NVIC_EnableIRQ(ADC_IRQn);
	flag1=0;
 for(i=0;i<9;i++)
 {
    temp1=init_command[i];
	lcd_write();
	}
 flag1=1;

	i=0;
	while(s[i]!='\0'){
		temp1=s[i];
		lcd_write();
		i++;
	}
	//flag1=0;
	//temp1=0x8C;
	//lcd_write();
	//flag1=1;
	
while(1){
channel=(LPC_ADC->ADGDR>>24)&0x07;
	if(channel==4){	
		temp=LPC_ADC->ADDR4;
		temp>>=4;
		temp&=0xFFF;
		result4=(float)temp*3.3/4096;
	}
	if(channel==5){	
		temp=LPC_ADC->ADDR5;
		temp>>=4;
		temp&=0xFFF;
		result5=(float)temp*3.3/4096;
	}
	result_an=result5-result4;
	result_dig=(int)result_an*4096/3.3;
	result_dig&=0xFFF;
	sprintf(dig,"%x",result_dig);
	i=0;
	while(dig[i]!='\0'){
		temp1=dig[i];
		lcd_write();
		i++;
	}
}
while(1);
}

 void lcd_write(void){
	flag2=(flag1==1)?0:((temp1==0x30)||(temp1==0x20))?1:0;
	temp2=temp1&0xF0;
	temp2<<=19;
	port_write();
	if(!flag2){
		temp2=temp1&0x0F;
		temp2<<=23;
		port_write();
	}
}
void port_write(void){
	LPC_GPIO0->FIOPIN=0;
	LPC_GPIO0->FIOPIN=temp2;
	if(flag1==0)
		LPC_GPIO0->FIOCLR=RS_CTRL;
	else
		LPC_GPIO0->FIOSET=RS_CTRL;
	LPC_GPIO0->FIOSET=EN_CTRL;
	delay_lcd(25);
	LPC_GPIO0->FIOCLR=EN_CTRL;
	delay_lcd(5000);
}
void delay_lcd(unsigned int r1){
	unsigned int r;
	for(r=0;r<r1;r++);
}