 AREA RESET,DATA,READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,=num
	LDR R2,[R0]
	MLA R1,R2,R2,R2
	LSR R3,R1,#1
	LDR R4,=res
	STR R3,[R4]
STOP B STOP
	AREA data1,DATA,READWRITE
num DCD 0x00000002	
res	DCD 0
	END