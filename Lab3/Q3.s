 AREA RESET,DATA,READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,=num1
	LDR R1,=num2
	LDR R2,[R0]
	LDR R3,[R1]
up
	CMP R2,R3
	BEQ exit
	SUBHI R2,R3
	SUBLO R3,R2
	B up
exit 
	LDR R0,=gcd
	STR R2,[R0]
STOP B STOP
	AREA data1,DATA,READWRITE
num1 DCD 0x00000000
num2 DCD 0x00000000
gcd	DCD 0
	END