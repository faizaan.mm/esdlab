 AREA RESET,DATA,READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,=num1
	LDR R1,=num2
	LDR R7, [R0]
	MOV R2,#0 ; Sum
	MOV R3,#0 ; Carry
	LDR R5,[R1] ; Loop Counter 
up
	ADDS R2,R7
	ADC R3,#0
	SUB R5,#1
	TEQ R5,#0
	BNE up
	LDR R6,=res
	STR R2,[R6] ,#4
	STR R3,[R6]
STOP B STOP
	AREA data1,DATA,READWRITE
num1 DCD 0x00000000
num2 DCD 0x00000000
res	DCD 0
	END