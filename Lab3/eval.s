 AREA RESET,DATA,READONLY
	EXPORT __Vectors
__Vectors
	DCD 0x10001000
	DCD Reset_Handler
	ALIGN
	AREA mycode,CODE,READONLY
	ENTRY
	EXPORT Reset_Handler
Reset_Handler
	LDR R0,=num1
	LDR R1,=num2
	LDR R2,=diff
	MOV R4,#2 ; loop counter
	LDR R3,=0x20000000
	MSR xPSR,R3	
up
	LDR R5,[R0],#4
	LDR R6,[R1],#4
	SBCS R7,R5,R6
	STR R7,[R2],#4
	SUB R4,#1
	TEQ R4,#0
	BNE up
STOP B STOP
num1 DCD 0xFFFFFAB9,0xFFFFFAB9
num2 DCD 0x11AA11AA,0x1B1B1B1B
	AREA data1,DATA,READWRITE
diff DCD 0
	END